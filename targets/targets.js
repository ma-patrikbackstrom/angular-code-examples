angular.module("modules.targets", [
  "targets.controllers"
]).config(["$stateProvider", function($stateProvider) {

  $stateProvider.state("targets", {
    abstract: true,
    url: "",
    parent: "feed.default",
    views: {
      "header@feed.default": {}
    },
    access: {
      type: "private",
      groups: "all"
    }
  }).state("targets.all", {
    url: "targets",
    views: {
      "main@": {
        controller: "allTargetsCtrl",
        templateUrl: "modules/targets/partials/allTargets.tpl.html"
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  }).state("targets.group", {
    url: "targets/group?gid",
    views: {
      "main@": {
        controller: "groupTargetsCtrl",
        templateUrl: "modules/targets/partials/groupTargets.tpl.html"
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  }).state("targets.one", {
    url: "targets/one?gid&tid&type",
    views: {
      "main@": {
        controller: "oneTargetCtrl",
        templateUrl: "modules/targets/partials/oneTarget.tpl.html"
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  }).state("targets.one.userReports", {
    url: "/userReports?uid",
    views: {
      "main@": {
        controller: "userReportsByTargetCtrl",
        templateUrl: "modules/targets/partials/userReportsByTarget.tpl.html"
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  }).state("targets.one.anonymousReports", {
    url: "/anonymousLoops",
    views: {
      "main@": {
        controller: "anonymousReportsByTargetCtrl",
        templateUrl: "modules/targets/partials/anonymousReportsByTarget.tpl.html"
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  }).state("targets.one.allLoopsByTag", {
    url: "/allLoopsByTag",
    views: {
      "main@": {
        controller: "allLoopsByTagCtrl",
        templateUrl: "modules/targets/partials/allLoopsByTag.tpl.html"
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  }).state("targets.manageTarget", {
    url: "targets/manage?gid&tid&mode&type&returnToCaller",
    views: {
      "main@": {
        controller: "manageTargetCtrl",
        templateUrl: "modules/targets/partials/manageTarget.tpl.html"
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  });
}]);
