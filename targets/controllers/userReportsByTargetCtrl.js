angular.module("targets.controllers").controller("userReportsByTargetCtrl", [
  "$scope",
  "$stateParams",
  "CONSTANTS",
  "apiService",
  function(
    $scope,
    $stateParams,
    CONSTANTS,
    apiService
  ) {
    $scope.loading = true;
    $scope.loadingReports = true;
    $scope.user = null;
    $scope.reports = [];
    $scope.noMoreReports = false;

    // var skip = 0;
    // var limit = 5;

    var api_getGroup = CONSTANTS.DEFAULT_OBJECT_CALLS.GROUP;
    var api_getUser = CONSTANTS.DEFAULT_OBJECT_CALLS.USER;
    var api_getTarget = CONSTANTS.DEFAULT_OBJECT_CALLS.TARGET;

    api_getGroup[1].args[0] = $stateParams.gid;

    apiService.dispatcher(api_getGroup).then(function(group) {
      $scope.group = group;
      $scope.isAdmin = utils.isAdmin($scope.$root.userid, $scope.group.admins_);
      $scope.isCurrentUser = $stateParams.uid === $scope.$root.currentUser._id;
      $scope.loading = false;

      if ($scope.group.my_role.can_see_target_overview || $scope.isCurrentUser) {
        getUser();
        getTarget();
        getReportsByTargetAndUser();
      }
    }, function() {
      $scope.loading = false;
      console.error("Failed to fetch group data.");
    });

    function getUser() {
      api_getUser[1].args[0] = $stateParams.uid;
      apiService.dispatcher(api_getUser).then(function(res) {
        $scope.user = res.data;
      });
    }

    function getTarget() {
      api_getTarget[1].args[0] = $stateParams.tid;
      apiService.dispatcher(api_getTarget).then(function(res) {
        $scope.target = res.data;
      });
    }

    function getReportsByTargetAndUser() {
      apiService.dispatcher(["getLoopsByTargetAndUser", {args: [$stateParams.gid, $stateParams.tid, $stateParams.uid]}]).then(function(res) {
        $scope.reports = res.data;
        angular.forEach($scope.reports, function(report, i) {
          var tempReport = report;
          $scope.reports[i] = {
            notifications: [],
            created: tempReport.created,
            sent_by_: tempReport.creator_,
            group_: $scope.group,
            loop_: tempReport,
            target_: $scope.target,
            type: "loop",
            recipients_: tempReport.recipients_,
            seen_: tempReport.seen_,
          };
        });
        $scope.loadingReports = false;
      });
    }
  }
]);
