angular.module("targets.controllers").controller("groupTargetsCtrl", [
  "$scope",
  "$stateParams",
  "$q",
  "CONSTANTS",
  "apiService",
  function(
    $scope,
    $stateParams,
    $q,
    CONSTANTS,
    apiService
  ) {
    $scope.loading = true;
    $scope.loadingTargets = true;

    var defaultGroupCall = CONSTANTS.DEFAULT_OBJECT_CALLS.GROUP;
    defaultGroupCall[1].args[0] = $stateParams.gid;

    initGroupTargets($stateParams.gid);

    function initGroupTargets(gid) {
      local_getGroup(gid).then(function(status) {
        $scope.targetsCompleted = 0;
        apiService.dispatcher(["getAllTargets", {args: [gid]}]).then(function(targets) {

          $scope.targets = utils.sortByKey(targets.data.filter(function(target) {
            return target.is_target === true;
          }), "name");

          $scope.tags = utils.sortByKey(targets.data.filter(function(target) {
            return target.is_target === false;
          }), "name");

          angular.forEach($scope.targets, function(target) {
            if (target.my_completed_count >= target.my_count) {
              $scope.targetsCompleted++;
            }
            // Calculate currentUser's (if can_complete_target) completion rate
            if ($scope.group.my_role.can_complete_target) {
              target.my_completion_percentage = Math.round(target.my_completed_count / target.my_count * 100);
            }
          });
          $scope.loadingTargets = false;
        },function(error) {
          console.log("targetsctrl::initGroupTargets::getAllTargets ", error);
          $scope.loadingTargets = false;
        });
      });
    }

    $scope.orderBy = function(arrayName, key, descending) {
      $scope[arrayName] = utils.sortByKey($scope[arrayName], key, descending);
    };

    /*
      NOTE: Unnecessary as separate function now that view for all targets,
      group specific target, and one target, are separated into their own
      controllers.
    */
    function local_getGroup(gid) {
      var deferred = $q.defer();
      apiService.dispatcher(defaultGroupCall).then(function(group) {
        $scope.group = group;
        $scope.group.isAdmin = utils.isAdmin($scope.$root.currentUser._id, $scope.group.admins_);
        $scope.group.isMember = utils.isGroupMember($scope.$root.currentUser._id, $scope.group.members_);

        $scope.hasTargets = utils.hasTargets(group);
        $scope.hasTags = utils.hasTags(group);

        $scope.loading = false;
        deferred.resolve(0);
      }, function(error) {
        console.log("targetsctrl::local_getGroup: ", error);
        $scope.loading = false;
        deferred.reject(-1);
      });
      return deferred.promise;
    }
  }
]);
