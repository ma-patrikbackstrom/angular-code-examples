angular.module("targets.controllers").controller("oneTargetCtrl", [
  "$scope",
  "$q",
  "$stateParams",
  "$timeout",
  "$translate",
  "$state",
  "$filter",
  "CONSTANTS",
  "ngDialog",
  "toaster",
  "apiService",
  function(
    $scope,
    $q,
    $stateParams,
    $timeout,
    $translate,
    $state,
    $filter,
    CONSTANTS,
    ngDialog,
    toaster,
    apiService
  ) {
    $scope.loading = true;
    $scope.loadingTarget = true;
    $scope.loadingProgress = true;
    $scope.loadingReports = true;

    var defaultGroupCall = CONSTANTS.DEFAULT_OBJECT_CALLS.GROUP;
    defaultGroupCall[1].args[0] = $stateParams.gid;

    init($stateParams.gid, $stateParams.tid);

    function init(gid, tid) {
      local_getGroup(gid).then(function(group) {
        $scope.group = group;

        if ($scope.group.my_role.can_complete_target) {
          getMyReportsByTarget(gid, tid);
        }
        getUsersProgress(gid, tid);

        // NOTE: API getTarget doesn't return necessary progress data...
        apiService.dispatcher(["getAllTargets", {args: [gid]}]).then(function(res) {
          $scope.target = res.data.filter(function(target) {
            return target._id === tid;
          })[0];

          if ($scope.target.is_target && $scope.group.my_role.can_see_target_overview) {
            drawPiechart();
          }

          $scope.target.type_string = $scope.target.is_target === true ? "target" : "tag";
          $scope.loadingTarget = false;
          $scope.loadingTargetError = false;
          $scope.loading = false;
        }, function(errerr) {
          console.log(errerr);
          $scope.loadingTarget = false;
          $scope.loadingTargetError = true;
          $scope.loading = false;
        });
      });
    }

    /*
    TODO: Get only 'myProgress' if current user is not Leeder. Otherwise
    all users' progress is available in http request... NOTE: No API for
    this yet!
    */
    function getUsersProgress(gid, tid) {
      apiService.dispatcher(["getUserProgressByTarget", {args: [gid, tid]}]).then(function(res) {
        $scope.userProgress = res.data;
        angular.forEach($scope.userProgress, function(userProgress) {
          getProfilePicture(userProgress.user || {});
        });
        $scope.myProgress = res.data[$scope.$root.currentUser._id];
        $scope.tid = tid;
        $scope.loadingProgress = false;
      },function(error) {
        console.log("targetsctrl::init::getUserProgressByTarget: ", error);
        $scope.loadingProgress = false;
      });
    }

    /**
      Method: getProfilePicture()
      Description:

      TODO: When iterating multiple users and calling getProfilePicture() on
      each user, anonymous users cannot be updated due to no angular hash in
      user object.
    */
    function getProfilePicture(user) {
      apiService.dispatcher(["getProfilePicture", {args: [user]}]).then(function(res) {
        user.profile_pic_s3_temp = res.profile_pic;
      }, function(err) {
        if (err.code === CONSTANTS.ERRORS.ANONYMOUS_USER || err.code === CONSTANTS.ERRORS.NO_USER_OBJECT_AVAILABLE || err.code === CONSTANTS.NO_USER_PROFILE_PICTURE) {
          user.profile_pic = CONSTANTS.DEFAULT_PROFILE_PICTURE;
        } else {
          // Silence error
          // console.error("UNCAUGHT ERROR", err);
          user.profile_pic = CONSTANTS.DEFAULT_PROFILE_PICTURE;
        }
      });
    }

    function getMyReportsByTarget(gid, tid) {
      apiService.dispatcher(["getMyLoopsByTarget", {args: [gid, tid]}]).then(function(res) {
        $scope.reports = res.data;
        angular.forEach($scope.reports, function(report, i) {
          var tempReport = report;
          $scope.reports[i] = {
            notifications: [],
            created: tempReport.created,
            sent_by_: tempReport.creator_,
            group_: $scope.group,
            loop_: tempReport,
            target_: $scope.target,
            type: "loop",
            recipients_: tempReport.recipients_,
            seen_: tempReport.seen_,
          };
        });
        $scope.loadingReports = false;
      });
    }

    function drawPiechart() {
      $timeout(function() {
        $scope.target.members_finished_percentage = Math.round($scope.target.members_finished / $scope.target.total_members_doing_targets * 100) + "%";
        var canvas = document.getElementById("piechart");
        var ctx = canvas.getContext("2d");
        var lastend = 0;
        var data = [$scope.target.total_members_doing_targets - $scope.target.members_finished, $scope.target.members_finished]; // If you add more data values make sure you add more colors
        var myTotal = 0; // Automatically calculated so don't touch
        var myColor = ['#e7e7e7', '#40c731']; // Colors of each slice
        var i;

        for (i = 0; i < data.length; i++) {
          myTotal += data[i];
        }

        ctx.translate(canvas.width/2, canvas.width/2);
        ctx.rotate(-90*Math.PI/180);
        ctx.translate(-canvas.width/2, -canvas.width/2);

        for (i = 0; i < data.length; i++) {
          ctx.fillStyle = myColor[i];
          ctx.beginPath();
          ctx.moveTo(canvas.width / 2, canvas.height / 2);
          ctx.arc(canvas.width / 2, canvas.height / 2, canvas.height / 2 - 2, -lastend, -lastend + -(Math.PI * 2 * (data[i] / myTotal)), true);
          ctx.lineTo(canvas.width / 2, canvas.height / 2);
          ctx.fill();
          lastend += Math.PI * 2 * (data[i] / myTotal);
        }

        ctx.strokeStyle = "#E4E4E4";
        ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.moveTo(canvas.width, canvas.height / 2);
        ctx.arc(canvas.width / 2, canvas.height / 2, (canvas.height / 2) - (ctx.lineWidth / 2), 0, Math.PI * 2, false);
        ctx.stroke();
        ctx.closePath();
      }, 200); // 200ms to reassure that element is drawn before accessing context
    }


    /*
      NOTE: Unnecessary as separate function now that view for all targets,
      group specific target, and one target, are separated into their own
      controllers.
    */
    function local_getGroup(gid) {
      var deferred = $q.defer();
      apiService.dispatcher(defaultGroupCall).then(function(group) {
        group.isAdmin = utils.isAdmin($scope.$root.currentUser._id, group.admins_);
        group.isMember = utils.isGroupMember($scope.$root.currentUser._id, group.members_);

        $scope.hasTargets = utils.hasTargets(group);
        $scope.hasTags = utils.hasTags(group);

        deferred.resolve(group);
      }, function(error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }

    $scope.downloadFile = function(path) {
      apiService.dispatcher(["getFileUrl", {args: [CONSTANTS.FILES_FOLDER, path]}]).then(function(res) {
        window.open(res.url);
      });
    };

    $scope.deleteTarget = function(tid) {
      ngDialog.openConfirm({
        template: "modules/targets/modals/confirm-delete.tpl.html",
        className: "ngdialog-theme-plain",
        showClose: false,
        scope: $scope
      }).then(function() {
        $translate([
          "v3.modules.targets.tag_deleted",
          "v3.modules.targets.target_deleted",
          "v3.modules.targets.tag_not_deleted",
          "v3.modules.targets.target_not_deleted"
        ]).then(function(translations) {
          var temp = $scope.$root.feed.length;
          for (var i = 0; i < $scope.$root.feed.length; i++) {
            if ($scope.$root.feed[i].type === "loop") {
              var continue_j = true;
              for (var j = 0; continue_j && j < $scope.$root.feed[i].loop_.targets_.length; j++) {
                if ($scope.$root.feed[i].loop_.targets_[j].target._id === tid) {
                  $scope.$root.feed.splice(i, 1);
                  continue_j = false;
                  i--;
                }
              }
            } else if ($scope.$root.feed[i].type === "target" && $scope.$root.feed[i].target_._id === tid) {
              $scope.$root.feed.splice(i, 1);
              i--;
            }
          }
          apiService.dispatcher(["deleteTarget", {args: [tid]}]).then(function(res) {
            toaster.pop("lmsuccess", $filter("capitalize")(translations["v3.modules.targets." + $scope.target.type_string + "_deleted"]), "", 5000, "trustedHtml");
            $state.go("targets.group", {gid: $scope.group._id});
          }, function(error) {
            toaster.pop("lmerror", $filter("capitalize")(translations["v3.modules.targets." + $scope.target.type_string + "_not_deleted"]), "", 10000, "trustedHtml");
            $logentries.error({
              "CAT": "target::delete",
              "CODE": error.code,
              "MESSAGE": error.message,
              "DATA": error.data,
              "USER": $scope.$root.currentUser._id,
              "ENVIRONMENT": utils.getUserEnvironment()
            });
          });
        });
      });
    };
  }
]);
