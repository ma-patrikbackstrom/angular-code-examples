angular.module("targets.controllers").controller("allTargetsCtrl", [
  "$scope",
  "$q",
  "$stateParams",
  "CONSTANTS",
  "apiService",
  function(
    $scope,
    $q,
    $stateParams,
    CONSTANTS,
    apiService
  ) {
    $scope.loading = true;

    var defaultGroupCall = CONSTANTS.DEFAULT_OBJECT_CALLS.GROUP;
    defaultGroupCall[1].args[0] = $stateParams.gid;

    init();

    function init() {
      apiService.dispatcher(["getMyGroups"]).then(function(groups) {
        $scope.groups = utils.sortByKey(groups.data, "name");
        /*
          $q.all(promises) to complete all requests before rendering DOM
        */
        var promises = [];
        angular.forEach($scope.groups, function(obj, index) {
          promises.push(apiService.dispatcher(["getAllTargets", {args: [obj._id]}]).then(function(res) {

            // Filter targets and sort on 'name'
            $scope.groups[index].targets_ = utils.sortByKey(res.data.filter(function(target) {
              return target.is_target === true;
            }), "name");

            // Calculate currentUser's (if can_complete_target) completion rate
            if ($scope.groups[index].my_role.can_complete_target) {
              angular.forEach($scope.groups[index].targets_, function(target) {
                target.my_completion_percentage = Math.round(target.my_completed_count / target.my_count * 100);
              });
            }
            return;
          }));
        });
        // Count number of mandatory targets in group AND set group as not open (for toggling GUI)
        $q.all(promises).then(function(_) {
          angular.forEach($scope.groups, function(group, index) {
            group.targetsCompleted = 0;
            angular.forEach(group.targets_, function(target) {
              if (target.my_completed_count >= target.my_count) {
                group.targetsCompleted++;
              }
            });
            group.isOpen = false;
            group.isAdmin = utils.isAdmin($scope.$root.userid, group.admins_);
            group.hasTargets = utils.hasTargets(group);
            group.hasTags = utils.hasTags(group);
            group.numberOfMandatoryTargets = 0;
            angular.forEach(group.targets_, function(target) {
              if (target.is_target) {
                group.numberOfMandatoryTargets++;
              }
            });
          });
          $scope.loading = false;
        });
      }, function(error) {
        console.log("targetsctrl::init::getMyGroups: ", error);
      });
    }

    $scope.orderBy = function(arrayName, key, descending, gid) {
      var index = utils.getIndexByKey("_id", $scope.groups, gid);
      $scope.groups[index][arrayName] = utils.sortByKey($scope.groups[index][arrayName], key, descending);
    };
  }
]);
