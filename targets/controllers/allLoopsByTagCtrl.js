angular.module("targets.controllers", []).controller("allLoopsByTagCtrl", [
  "$scope",
  "$stateParams",
  "CONSTANTS",
  "apiService",
  function(
    $scope,
    $stateParams,
    CONSTANTS,
    apiService
  ) {
    /*
      NOTE: Module exactly the same as anonymousReportsByTargetController apart
      from API call. Also very similar to userReportsByTargetCtrl. Merge these
      modules and use $stateParams.anonymous to decide what API call to use.
    */
    $scope.loading = true;
    $scope.loadingLoops = true;
    $scope.loops = [];

    var api_getGroup = CONSTANTS.DEFAULT_OBJECT_CALLS.GROUP;
    var api_getTarget = CONSTANTS.DEFAULT_OBJECT_CALLS.TARGET;

    api_getGroup[1].args[0] = $stateParams.gid;

    apiService.dispatcher(api_getGroup).then(function(group) {
      $scope.group = group;
      $scope.isAdmin = utils.isAdmin($scope.$root.userid, $scope.group.admins_);
      $scope.loading = false;

      if ($scope.group.my_role.can_see_target_overview) {
        getTarget();
        getLoopsByTag();
      }
    }, function() {
      $scope.loading = false;
      console.error("Failed to fetch group data.");
    });

    function getTarget() {
      api_getTarget[1].args[0] = $stateParams.tid;
      apiService.dispatcher(api_getTarget).then(function(res) {
        $scope.target = res.data;
      });
    }

    function getLoopsByTag() {
      apiService.dispatcher(["getLoopsByTag", {args: [$stateParams.gid, $stateParams.tid, $stateParams.uid]}]).then(function(res) {
        $scope.loops = res.data;

        // Fake loops as feedentry objects
        angular.forEach($scope.loops, function(report, i) {
          var tempReport = report;
          $scope.loops[i] = {
            notifications: [],
            created: tempReport.created,
            sent_by_: tempReport.creator_,
            group_: $scope.group,
            loop_: tempReport,
            target_: $scope.target,
            type: "loop",
            recipients_: tempReport.recipients_,
            seen_: tempReport.seen_,
          };
        });
        $scope.loadingLoops = false;
      });
    }
  }
]);
