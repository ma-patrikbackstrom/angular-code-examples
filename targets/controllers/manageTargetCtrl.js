angular.module("targets.controllers").controller("manageTargetCtrl", [
  "$scope",
  "$state",
  "$stateParams",
  "$translate",
  "$filter",
  "$logentries",
  "$q",
  "CONSTANTS",
  "ngDialog",
  "toaster",
  "apiService",
  function(
    $scope,
    $state,
    $stateParams,
    $translate,
    $filter,
    $logentries,
    $q,
    CONSTANTS,
    ngDialog,
    toaster,
    apiService
  ) {
    $scope.loading = true;
    $scope.targetData = {count: 0, needs_approval: true, files: []};
    $scope.mode = $stateParams.mode;
    $scope.type = $stateParams.type;
    $scope.numberOfFilesAllowed = -1; // -1 = unlimited amount of files.
    $scope.tempFiles = []; // Temporary storage for files.
    $scope.allowedFiletypes = CONSTANTS.FILE_MATCH_EXTENSION_FORMATTED;

    if ($stateParams.returnToCaller) {
      if ($scope.$root.resolveFromState() === -1) {
        $scope.returnToDefault = true;
      } else {
        $scope.returnString = $scope.$root.resolveFromState();
        $scope.returnObject = $scope.$root.resolveFromStateAsObject();
      }
    }

    if ($stateParams.type === "tag") {
      $scope.numberOfFilesAllowed = 0;
    }

    var defaultGroupCall = CONSTANTS.DEFAULT_OBJECT_CALLS.GROUP;
    defaultGroupCall[1].args[0] = $stateParams.gid;

    local_getGroup($stateParams.gid);

    if ($stateParams.tid) {
      apiService.dispatcher(["getTarget", {args: [$stateParams.tid]}]).then(function(res) {
        $scope.targetData = res.data;
        $scope.lowestCountLimit = $scope.targetData.count; // 'count' of already existing target. Updated target count cannot be lower
        $scope.limitCount = true;
      });
    }

    function local_getGroup(gid) {
      apiService.dispatcher(defaultGroupCall).then(function(group) {
        $scope.group = group;
        $scope.group.isAdmin = utils.isAdmin($scope.$root.userid, $scope.group.admins_);
        $scope.loading = false;
      },function(error) {
        console.log("managetargetctrl::local_getGroup: ", error);
        $scope.loading = false;
      });
    }

    $scope.removeFile = function(file) {
      console.log(file);
      var found = false;
      var i = 0;
      for (i = 0; i < $scope.targetData.files.length && !found; i++) {
        if ($scope.targetData.files[i].$$hashKey === file.$$hashKey) {
          found = true;
          $scope.targetData.files.splice(i, 1);
        }
      }
      for (i = 0; i < $scope.tempFiles.length && !found; i++) {
        if ($scope.tempFiles[i].$$hashKey === file.$$hashKey) {
          found = true;
          $scope.tempFiles.splice(i, 1);
        }
      }
    };

    var handleFileSelect = function(evt) {
      $scope.wrongFileType = false;
      var file = evt.currentTarget.files[0];
      var reader = new FileReader();

      reader.onload = onload(evt);

      function onload(evt) {
        if (file.name.match(CONSTANTS.FILE_MATCH_EXTENSION) === null) {
          $scope.$apply(function($scope){
            $scope.wrongFileType = true;
            $scope.selectedFileType = file.name.substr((~-file.name.lastIndexOf(".") >>> 0) + 2);
          });
        } else {
          $scope.$apply(function($scope){
            $scope.wrongFileType = false;
            $scope.tempFiles.push(file);
          });
        }
      }

      reader.readAsDataURL(file);
    };
    $(document).on('change', '#fileInput', handleFileSelect);

    $scope.saveTarget = function(form, targetData) {
      if (form.$valid) {
        $scope.posting = true;
        prepareAndSendFiles(form, targetData);
      }
    };

    function prepareAndSendFiles(form, targetData) {
      ngDialog.openConfirm({
        template: "common/partials/modals/confirm.tpl.html",
        className: "ngdialog-theme-plain",
        showClose: false
      }).then(function() {
        if ($scope.tempFiles.length > 0) {
          var promises = [];
          var fileUploadFailed = function(error) {
            if (error.code === CONSTANTS.ERRORS.COULD_NOT_UPLOAD_FILE) {
              toaster.pop({
                type: "lmerror",
                title: error.msg,
                body: error.code,
                timeout: 5000
              });
            } else {
              console.error(error);
            }
          };

          for (var i = 0; i < $scope.tempFiles.length; i++) {
            promises.push(apiService.dispatcher(["uploadFile", {args: [CONSTANTS.FILES_FOLDER, $scope.tempFiles[i]]}]).then(function(res) {
              return res;
            }, fileUploadFailed));
          }

          $q.all(promises).then(function(newFiles) {
            prepareAndSendTarget(form, targetData, newFiles);
          }, function(error) {
            toaster.pop({
              type: "lmerror",
              title: "One or more files couldn't be uploaded",
              body: "ERROR " + error.status + ": " + error.data.message,
              timeout: 5000
            });
          });
        } else {
          prepareAndSendTarget(form, targetData, null);
        }
      }, function() {
        $scope.posting = false;
      });
    }

    function prepareAndSendTarget(form, targetData, newFiles) {

      var apiCall = null;

      if (newFiles) {
        newFiles.map(function(file) {
          targetData.files.push({path: file.fullPath, name: file.name});
        });
      }

      if ($scope.group._id) {
        targetData.group_id = $scope.group._id;
      }

      if ($stateParams.tid) {
        targetData.id = $stateParams.tid;
      }

      if (targetData.followup === "") {
        delete targetData.followup;
      }

      if ($scope.type === "target") {
        targetData.is_target = true;
      } else {
        targetData.is_target = false;
        targetData.count = 0;
      }

      targetData.desc = targetData.desc ? targetData.desc : "";
      targetData.visibility = "11111"; // TODO: Shall be removed in backend
      targetData.shown_as_goal = false; // TODO: Shall be removed in backend
      targetData.type = "reflection"; // TODO: Shall be removed in backend

      if ($scope.mode === "create") {
        apiCall = ["postTarget", {args: [targetData]}];
      } else {
        apiCall = ["putTarget", {args: [targetData]}];
      }

      $translate([
        "v3.modules.targets." + $scope.type + "_was_created",
        "v3.modules.targets." + $scope.type + "_was_not_created",
        "v3.modules.targets." + $scope.type + "_was_updated",
        "v3.modules.targets." + $scope.type + "_was_not_updated"
      ], {targetname: $scope.targetData.name}).then(function(translations) {
        apiService.dispatcher(apiCall).then(function(res) {
          toaster.pop({
            type: "lmsuccess",
            body: $filter("capitalize")(translations["v3.modules.targets." + $scope.type + "_was_created"]),
            timeout: 5000
          });

          $state.go("targets.one", {gid: $scope.group._id, tid: res.data._id});

        }, function(error) {
          toaster.pop({
            type: "lmerror",
            body: $filter("capitalize")(translations["v3.modules.targets." + $scope.type + "_was_not_created"]),
            timeout: 5000
          });
          $logentries.error({
            "CAT": "target::" + $scope.mode + "-" + $scope.type,
            "CODE": error.code,
            "MESSAGE": error.message,
            "DATA": error.data,
            "USER": $scope.$root.currentUser._id,
            "ENVIRONMENT": utils.getUserEnvironment()
          });
        });
      });
    }

    $scope.$watch(
      function watch(scope) {
        return (scope.targetData.count);
      }, function handle(newVal, oldVal) {
        if ($scope.limitCount && newVal < $scope.lowestCountLimit) {
          $scope.targetData.count = $scope.lowestCountLimit;
          $scope.showCountLimitInfo = true;
        } else if (newVal < 1) {
          $scope.targetData.count = 1;
        }
      }
    );
  }
]);
