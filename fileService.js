angular.module("common.services").factory("fileService", [
  "$q",
  "CONSTANTS",
  "collectiveService",
  "userService",
  function(
    $q,
    CONSTANTS,
    collectiveService,
    userService
  ) {
    var fileService = {};

    var bucket = null;
    var s3 = null;

    init();

    function init() {
      AWS.config.update({accessKeyId: CONSTANTS.AWS_CREDS.ACCESS_KEY, secretAccessKey: CONSTANTS.AWS_CREDS.SECRET_KEY});
      AWS.config.region = CONSTANTS.AWS_CREDS.REGION;
      bucket = new AWS.S3({params: {Bucket: CONSTANTS.AWS_CREDS.BUCKET_NAME}});
      s3 = new AWS.S3();
    }

    fileService.addProfilePicture = function(folder, file) {
      var deferred = $q.defer();
      if (!file) {
        deferred.reject(CONSTANTS.ERRORS.NO_FILE_SELECTED);
      } else if (!file.type.match(/image.*/)) {
        deferred.reject(CONSTANTS.ERRORS.WRONG_FILE_TYPE);
      } else {
        fileService.uploadFile(folder, file).then(function(res) {
          userService.linkProfilePicture(res.fullPath).then(function(subres) {
            deferred.resolve(subres);
          }, function(err) {
            fileService.removeProfilePicture(null, res.fullPath);
            deferred.reject(err);
          });
        }, function(err) {
          deferred.reject(err);
        });
      }
      return deferred.promise;
    };

    fileService.removeProfilePicture = function(folder, fileName) {
      return fileService.removeFile(folder, fileName);
    };

    fileService.getProfilePicture = function(user) {
      var deferred = $q.defer();
      if (!user || (Object.keys(user).length === 0 && user.constructor === Object)) {
        deferred.reject(CONSTANTS.ERRORS.NO_USER_OBJECT_AVAILABLE);
      } else if (user.name.toLowerCase() === "anonymous") {
        deferred.reject(CONSTANTS.ERRORS.ANONYMOUS_USER);
      } else {
        if (user.profile_pic !== null || user.profile_pic !== undefined || user.profile_pic !== "") {
          getFileUrl(user.profile_pic).then(function(res) {
            deferred.resolve({profile_pic: res.url});
          }, function(err) {
            // TODO: Remove db reference to profile picture in user object
            // NOTE: Not implemented in backend
            // userService.unlinkProfilePicture();
            deferred.reject(err);
          });
        } else {
          deferred.reject(CONSTANTS.ERRORS.NO_USER_PROFILE_PICTURE);
        }
      }
      return deferred.promise;
    };

    /*
      Method: uploadFile
    */
    fileService.uploadFile = function(folder, file) {
      var deferred = $q.defer();
      var fullPath = "";

      if (file) {
        var cleanName = file.name ? runRegexWhiteList(file.name) : "no_name";
        fullPath += fileService.trimFolderName(folder);
        fullPath += collectiveService.generateUUID() + "_" + Date.now() + CONSTANTS.FILE_NAME_DELIMITER + cleanName;

        var params = {Key: fullPath, ContentType: file.type, Body: file, ServerSideEncryption: "AES256"};

        bucket.putObject(params, function(err, data) {
          if (err) {
            deferred.reject(err);
          } else {
            deferred.resolve({fullPath: fullPath, name: file.name ? file.name : "no_name"}); // Return original name or no_name!
          }
        });
      } else {
        deferred.reject(CONSTANTS.ERRORS.NO_FILE_SELECTED); // No file selected
      }
      return deferred.promise;
    };

    /*
      Method: removeFile
      Expects 'folder === null' if 'fileName' is full path.
    */
    fileService.removeFile = function(folder, fileName) {
      var deferred = $q.defer();
      var params = {Bucket: CONSTANTS.AWS_CREDS.BUCKET_NAME, Key: null};

      params.Key = fileService.trimFolderName(folder);
      params.Key += fileName;

      bucket.deleteObject(params, function(err, data) {
        if (err) {
          deferred.reject(err);
        } else {
          deferred.resolve(data);
        }
      });
      return deferred.promise;
    };

    /*
      Method: getFileUrl
      Exported getFileUrl.
    */
    fileService.getFileUrl = function(folder, fileName) {
      return getFileUrl(fileName);
    };

    /*
      Method: getFileUrl
      Check if file 'filename' exist on S3 Bucket. If true, resolve signed url.
      Else, reject.
    */
    function getFileUrl(fileName) {
      var deferred = $q.defer();
      var params = {Bucket: CONSTANTS.AWS_CREDS.BUCKET_NAME, Key: fileName};

      bucket.headObject(params, function (err, data) {
        if (err) {
          deferred.reject(CONSTANTS.ERRORS.FILE_DOES_NOT_EXIST);
        } else {
          bucket.getSignedUrl("getObject", params, function(err, url) {
            if (err) {
              deferred.reject(err);
            } else {
              deferred.resolve({url: url});
            }
          });
        }
      });
      return deferred.promise;
    }

    /*
      Method: trimFolderName(string)
      Returns empty string if:
        string === null, or
        string === undefined, or
        string === "".
      Else,
        1. Run string through whitelist regex,
        2. Remove leading and trailing slashes,
        3. Remove multiple occurences of slashes (e.g. foo////bar = foo/bar),
        4. Add trailing slash to make it a folder
    */
    fileService.trimFolderName = function(folder) {
      if (folder === null || folder === undefined || folder === "") {
        return "";
      } else {
        return runRegexWhiteList(folder)
          .replace(CONSTANTS.FILE_NAME_REMOVE_LEADING_AND_TRAILING_SLASHES, "")
          .replace(CONSTANTS.FILE_NAME_REPLACE_MULTIPLE_SLASHES, '/') +
          "/";
      }
    };

    /*
      Function: runRegexWhiteList(string)
      Replaces whitespaces with underscores, and return 'string' run through
      whitelist regex.
    */
    function runRegexWhiteList(string) {
      return string
        .replace(/ /, "_")
        .replace(CONSTANTS.FILE_NAME_CHARACTER_WHITELIST, "");
    }

    return fileService;
  }
]);
