angular.module("common.services").factory("targetService", [
  "$http",
  "$q",
  "$state",
  "CONSTANTS",
  function(
    $http,
    $q,
    $state,
    CONSTANTS
  ) {
    var targetService = {};

    targetService.getAllTargets = function(gid) {
      return $http.get(CONSTANTS.API_IP + "/mytargets/v3/" + gid);
    };

    targetService.getUserProgressByGroup = function(gid) {
      return $http.get(CONSTANTS.API_IP + "/userprogress/" + gid);
    };

    targetService.getUserProgressByTarget = function(gid, tid) {
      return $http.get(CONSTANTS.API_IP + "/userprogress/" + gid + "/" + tid);
    };

    targetService.getTarget = function(tid) {
      return $http.get(CONSTANTS.API_IP + "/singeltarget/" + tid); // NOTE: singeltarget, not singletarget
    };

    targetService.getLoopsByTargetAndUser = function(gid, tid, uid) {
      return $http.get(CONSTANTS.API_IP + "/target-loops-by-user/" + gid + "/" + tid + "/" + uid);
    };

    targetService.getMyLoopsByTarget = function(gid, tid, uid) {
      return $http.get(CONSTANTS.API_IP + "/target-loops/" + gid + "/" + tid);
    };

    targetService.getAnonymousLoopsByTarget = function(gid, tid, uid) {
      return $http.get(CONSTANTS.API_IP + "/anonymous-loops/" + gid + "/" + tid);
    };

    targetService.getLoopsByTag = function(gid, tid, uid) {
      return $http.get(CONSTANTS.API_IP + "/tagged-loops/" + tid);
    };

    targetService.postTarget = function(target) {
      return $http.post(CONSTANTS.API_IP + "/targets", target);
    };

    targetService.putTarget = function(target) {
      return $http.put(CONSTANTS.API_IP + "/targets", target);
    };

    targetService.deleteTarget = function(tid) {
      return $http.delete(CONSTANTS.API_IP + "/targets/" + tid);
    };

    targetService.approveLoop = function(lid, tid) {
      return $http.put(CONSTANTS.API_IP + "/approve", {loop_id: lid, target_id: tid});
    };

    return targetService;
  }
]);
